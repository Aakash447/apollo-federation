const { ApolloServer } = require('apollo-server-express');
const { createServer } = require('http');
const cors = require('cors');
const express = require('express');
const app = express();
const typeDefs = require('../graphql/schemas');
const resolvers = require('../graphql/resolvers');
const context = require('../graphql/context');
const { buildFederatedSchema } = require('@apollo/federation');


app.use(cors());

const apolloServer = new ApolloServer({
    // schema: buildFederatedSchema[{
        // typeDefs, resolvers, context,
    // }]
    typeDefs,
    resolvers,
    context,
    introspection: true,
    playground: {
        settings: {
            'schema.polling.enable' : false,
        }
    }
});

apolloServer.applyMiddleware({ app, path: '/api' });

server = createServer(app);

module.exports = server;