module.exports = (sequelize, DataTypes) => {
  const Event = sequelize.define(
    'Event',
    {
      userId: DataTypes.INTEGER,
      eventName: DataTypes.STRING,
      description: DataTypes.STRING,
      eventDate: DataTypes.DATE,
      deleteEventDate: DataTypes.DATE,
      completedAt: DataTypes.DATE
    },
    {},
  );
  Event.associate = function (models) {
    Event.hasMany(models.GuestList, { foreignKey: 'eventId', as: 'events' });
    Event.belongsTo(models.User, { foreignKey: 'userId', as: 'creator' });
  };
  return Event;
}