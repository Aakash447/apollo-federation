module.exports = (sequelize, DataTypes) => {
  const GuestList = sequelize.define(
    'GuestList',
    {
      eventId: DataTypes.INTEGER,
      email: DataTypes.STRING,
    },
    {},
  );
  GuestList.associate = function (models) {
    GuestList.belongsTo(models.Event, { foreignKey: 'eventId', as: 'events' });
  };
  return GuestList;
}