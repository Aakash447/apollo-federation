require('dotenv').config();

const { ApolloServer } = require('apollo-server-express');
const { createServer } = require('http');
const cors = require('cors');
const express = require('express');
const app = express();
const typeDefs = require('./graphql/schemas/event');
const resolvers = require('./graphql/resolvers/Event');
// const context = require('./graphql/context');
// const schemaDirectives = require('./graphql/directives');
// const { buildFederatedSchema } = require('@apollo/federation');
const { buildSubgraphSchema } = require('@apollo/subgraph');


app.use(cors());
console.log("1");
// const schema = buildFederatedSchema({ typeDefs, resolvers });
const apolloServer = new ApolloServer({
  schema: buildSubgraphSchema({ typeDefs, resolvers }),
  context: ({ req, res }) => ({ req }),
  // schemaDirectives,
  introspection: true,
  playground: {
    settings: {
      'schema.polling.enable': false,
    }
  }
});
console.log("2");
// const apolloServer = new ApolloServer({
//   schema: buildSubgraphSchema([{
//     typeDefs,
//     resolvers,
//     context: ({ req, res }) => ({ req }),
//     // schemaDirectives,
//     // Subscriptions: {
//     //   onConnect: () => console.log("WS Connected!"),
//     //   onDisconnect: () => console.log("WS Disconnected!")
//     // },
//     introspection: true,
//     playground: {
//       settings: {
//         'schema.polling.enable': false,
//       }
//     }
//   }])
// });
console.log("3");
apolloServer.applyMiddleware({ app, path: '/api' });

server = createServer(app);

// apolloServer.installSubscriptionHandlers(server);

server.listen(9000, () => {
  console.log(`server created at 9000`);
  console.log(`subscription server created at ws:localhost:9000/api`);

});
