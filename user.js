require('dotenv').config();

const { ApolloServer } = require('apollo-server-express');
const { createServer } = require('http');
const cors = require('cors');
const express = require('express');
const app = express();
const typeDefs = require('./graphql/schemas/user');
const resolvers = require('./graphql/resolvers/User');
const context = require('./graphql/context');
// const schemaDirectives = require('./graphql/directives');
// const { buildFederatedSchema } = require('@apollo/federation');
const { buildSubgraphSchema } = require('@apollo/subgraph');


app.use(cors());
console.log("1");
// const schema = buildFederatedSchema({ typeDefs, resolvers });
const apolloServer = new ApolloServer({
  schema: buildSubgraphSchema({ typeDefs, resolvers }),
  context: ({ req, res }) => ({ req }),
  // schemaDirectives,
  introspection: true,
  playground: {
    settings: {
      'schema.polling.enable': false,
    }
  }
});
console.log("2");
// const apolloServer = new ApolloServer({
//   schema: buildSubgraphSchema([{
//     typeDefs,
//     resolvers,
//     context: ({ req, res }) => ({ req }),
//     // schemaDirectives,
//     // Subscriptions: {
//     //   onConnect: () => console.log("WS Connected!"),
//     //   onDisconnect: () => console.log("WS Disconnected!")
//     // },
//     introspection: true,
//     playground: {
//       settings: {
//         'schema.polling.enable': false,
//       }
//     }
//   }])
// });
console.log("3");
apolloServer.applyMiddleware({ app, path: '/api' });

server = createServer(app);

// apolloServer.installSubscriptionHandlers(server);

server.listen(process.env.PORT, () => {
  console.log(`server created at ${process.env.PORT}`);
  console.log(`subscription server created at ws:localhost:${process.env.PORT}/api`);

});
