require('dotenv').config();

const { ApolloServer } = require('apollo-server-express');
const { ApolloGateway } = require('@apollo/gateway');
const { createServer } = require('http');
const express = require('express');
const app = express();

const gateway = new ApolloGateway({
  serviceList: [{ name: "user", url: "http://localhost:5000/api" },
{ name: "event", url: "http://localhost:9000/api"}]
})

const apolloServer = new ApolloServer({
  gateway,
  subscriptions: false,
});
// const url = "http://localhost:6000/api";
console.log("ok123");

apolloServer.applyMiddleware({ app, path: '/api' });

server = createServer(app);

// apolloServer.installSubscriptionHandlers(server);

server.listen(process.env.GPORT, () => {
  console.log(`server created at http://localhost:${process.env.GPORT}/api`);
  // console.log(`subscription server created at ws:localhost:${process.env.PORT}/api`);

});