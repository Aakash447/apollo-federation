const { PubSub } = require('apollo-server-express');

const pubsub = new PubSub();

const pubsubTopics = { event: "EVENT" };

module.exports = { pubsub, pubsubTopics };
