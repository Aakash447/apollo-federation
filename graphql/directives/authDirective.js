const { AuthenticationError, SchemaDirectiveVisitor } = require('apollo-server-express');
const { defaultFieldResolver } = require('graphql');
const verifyToken = require('../context/index');

class AuthDirectives extends SchemaDirectiveVisitor {

  visitFieldDefinition(field) {
    const { resolve = defaultFieldResolver } = field;

    field.resolve = async function (...args) {
      let [_, { }, { req }] = args;
      
      const authToken = req.headers.authorization || '';
      // const token = await authToken.split(" ")[1];
      const authUser = await verifyToken(authToken);
      if (authUser) {
        req.user = authUser;
        const result = await resolve.apply(this, args);
        return result;
      } else {
        throw new AuthenticationError('login first');
      }
    }
  }
}

module.exports = AuthDirectives;