const {
  FilesystemUploader,
 } = require('./lib/gql-uploader');
 
 const fsFileUploader = new FilesystemUploader({ 
   dir: "temp/", 
   filenameTransform: filename => `${Date.now()}_${filename}`, 
 });
 
 module.exports = { fileUploader: fsFileUploader };