const { AuthenticationError } = require('apollo-server-express');
const { GuestList } = require("../../../database/models");
const { Event, User } = require("../../../database/models");


const createInvite = async (parent, args, { req }) => {
  try {
    const { eventId, email, userId = req.user.id } = args.input;
    const eventCheck = Event.findOne({ where: { id: eventId, userId: userId } });
    const userCheck = User.findOne({ where: { email: email } });
    if (!eventCheck) {
      return new AuthenticationError('Not Authorized.');
    }
    if (!userCheck) {
      return new AuthenticationError('Email not found.')
    }
    const GuestListInstance = { eventId, email };
    return await GuestList.create(GuestListInstance);
  } catch (error) {
    throw error;
  }
}

module.exports = createInvite;