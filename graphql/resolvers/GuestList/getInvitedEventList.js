const { AuthenticationError } = require('apollo-server-express');
const { GuestList } = require("../../../database/models");

const getInvitedEventList = async (parent, args, { req }) => {
  try {
    return await GuestList.findAll({ where: { email: req.user.email }, include: ['events'] });

  } catch (error) {
    throw error;
  }
}

module.exports = getInvitedEventList;