const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const { ApolloError, AuthenticationError } = require('apollo-server-express');
const { User } = require("../../../database/models");
const logger  = require('../../../services/logger');

const login = async (parent, args, context) => {
  try {
    const { email, password } = args.input;
    const auth = await User.findOne({ where: { email: email } });
    if (!auth) {
      return new ApolloError('register first')
    }
    if (bcrypt.compareSync(password, auth.password)) {
      const token = jwt.sign({ id: auth.id }, process.env.SECRET);

      logger.info('MUTATION Login - USER FOUND & Token Generated');

      const user = auth.toJSON();
      return { ...user, token };
    } else {
      logger.error('MUTATION Login - Invalid credentials');
      throw new AuthenticationError('Invalid credentials');
    }
  } catch (error) {
    throw error;
  }
}

module.exports = login;