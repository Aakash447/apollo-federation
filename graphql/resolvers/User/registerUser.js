const { ApolloError } = require('apollo-server-express');
const { User } = require("../../../database/models");
// const NewUser = "new_user";


const createUser = async (parent, args, context) => {
  try {
    const { name, email, password } = args.input;
    // console.log(models);
    console.log(args.input);
    // pubsub.publish(NewUser, {
    //   name,
    //   email,
    //   password
    // });
    const UserInstance = { name, email, password };
    const check = await User.findOne({ where: { email: email } });
    //check User is already available or not
    if (check) {
      throw new ApolloError("User already exists")
    }
    console.log("1234");
    const userCreated = await User.create(UserInstance);
    console.log(userCreated);
    return userCreated;
  }
  catch (error) {
    throw error;
  }
}

module.exports = createUser;