const { AuthenticationError } = require('apollo-server-express');
const { User } = require("../../../database/models");
const bcrypt = require('bcryptjs');
const logger = require('../../../services/logger');

const resetPassword = async (parent, args, context) => {
  try {
    const { email, newPassword, confirmNewPassword } = args.input;
    const userCheck = await User.findOne({ where: { email: email } });
    if (!userCheck) {
      logger.error('MUTATION resetPassword - User not found', {
        user: email,
      });
      throw new AuthenticationError("user not found");
    }
    if (newPassword != confirmNewPassword) {
      logger.error("MUTATION resetPassword - newPassword and confirmNewPassword doesn't match.", {
        user: email
      });
      throw new AuthenticationError("newPassword and confirmNewPassword doesn't match.");
    }
    const newPass = await bcrypt.hash(newPassword, 10);
    userCheck.password = newPass;
    const user = await userCheck.save();

    logger.info('MUTATION resetPassword - USER Password Updated', {
      user: user.id,
    });

    return user;
  } catch (error) {
    throw error;
  }
}

module.exports = resetPassword;