const { AuthenticationError } = require('apollo-server-express');
const { User } = require("../../../database/models");
const bcrypt = require('bcryptjs');
const logger = require('../../../services/logger');

const changePassword = async (parent, args, { req }) => {
  try {
    const { oldPassword, newPassword, confirmNewPassword } = args.input;

    const userCheck = await User.findOne({ where: { id: req.user.id } });
    if(!userCheck) {
      logger.error('MUTATION changePassword - User not found', {
        user: req.user.email
      });

      return new AuthenticationError('User not found.')
    }
    if (newPassword != confirmNewPassword) {
      logger.error("MUTATION changePassword - newPassword and confirmNewPassword doesn't match.", {
        user: req.user.email
      });
      return new AuthenticationError("newPassword and confirmNewPassword doesn't match.");
    }
    if (bcrypt.compareSync(oldPassword, userCheck.password)) {
      const newPass = await bcrypt.hash(newPassword, 10);
      userCheck.password = newPass;
      const user = await userCheck.save();
      logger.info('MUTATION changePassword - USER Password Updated', {
        user: req.user.id,
      });  
      return user;
    } else {
      logger.error("MUTATION changePassword - oldPassword doesn't match.", {
        user: req.user.email
      });
      return new AuthenticationError("oldPassword doesn't match.");
    }
  } catch (error) {
    throw error;
  }
}

module.exports = changePassword;