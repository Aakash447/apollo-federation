const { ApolloError } = require('apollo-server-express');
const { User } = require("../../../database/models");
// const NewUser = "new_user";


const createUser = async (parent, args, context) => {
  try {
    const id = parent;
    // console.log(models);
    // console.log(args.input);
    // pubsub.publish(NewUser, {
    //   name,
    //   email,
    //   password
    // });
    const check = await User.findOne({ where: { id } });
    //check User is already available or not
    if (!check) {
      throw new ApolloError("User doesn't exists")
    }
    return check;
  }
  catch (error) {
    throw error;
  }
}

module.exports = createUser;