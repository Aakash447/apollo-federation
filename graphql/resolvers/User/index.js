const registerUser = require('./registerUser');
const login = require('./login');
const changePassword = require('./changePassword');
const resetPassword = require('./resetPassword');
const getUser = require('./getUser');
const getUserById = require('./getUserById');
const { createResolver } = require('graphql-tools/dist/stitching/makeRemoteExecutableSchema');


module.exports = {
  Mutation: {
    registerUser,
    login,
    changePassword,
    resetPassword,
  },
  Query: {
    getUser,
  },
  user: {
    async __resolveReference(user){
      // console.log("ok", user);
      return await getUserById(user.id);
    }
  }
}