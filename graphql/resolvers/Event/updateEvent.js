const { AuthenticationError } = require('apollo-server-express');
const { Event } = require("../../../database/models");

const updateEvent = async (parent, args, { req }) => {
  try {
    const { eventId, newEventName, newDescription } = args.input;
    const eventCheck = await Event.findByPk(eventId);
    if (!eventCheck) {
      throw new AuthenticationError('event not found');
    }
    if(eventCheck.userId != req.user.id) {
      throw new AuthenticationError('Not Authorized')
    }
    eventCheck.eventName = newEventName;
    eventCheck.description = newDescription;
    return await eventCheck.save();
  } catch (error) {
    throw error;
  }
}

module.exports = updateEvent;