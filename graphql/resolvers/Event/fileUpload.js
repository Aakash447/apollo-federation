const { fileUploader } = require('../../uploader');


const fileUpload= async (_, { file }) => {
  const { createReadStream, filename, mimetype, encoding } = await file;

  console.log('INPUT:', filename, mimetype, encoding);

  const uri = await fileUploader.upload(createReadStream(), {
    filename,
    mimetype,
  });

  return {
    filename,
    mimetype,
    encoding,
    uri,
  };
}


module.exports = fileUpload;