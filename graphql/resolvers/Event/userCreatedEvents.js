const db = require('../../../database/models/index');
const { Event } = require("../../../database/models");


const userCreatedEvents = async (parent, args, { req }) => {
  // const query = `SELECT "Event"."eventName","GuestList"."eventId",JSON_AGG("GuestList"."email") AS "guests" FROM "Events" AS "Event" JOIN "GuestLists" AS "GuestList" ON "Event"."id"="GuestList"."eventId" WHERE "Event"."userId"='${req.user.id}' GROUP BY "GuestList"."eventId","Event"."eventName"`;
  const query = `SELECT "Event"."eventName","GuestList"."eventId",JSON_AGG("GuestList"."email") AS "guests" FROM "Events" AS "Event" JOIN "GuestLists" AS "GuestList" ON "Event"."id"="GuestList"."eventId" WHERE "Event"."userId"=2 GROUP BY "GuestList"."eventId","Event"."eventName"`;
  // return await db.sequelize.query(
  //   query,
  //   {
  //     type: db.sequelize.QueryTypes.SELECT
  //   }
  // );
  return await Event.findAll({ where: { userId: 2 }, include: "events"});
}

module.exports = userCreatedEvents;