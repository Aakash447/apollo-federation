const createEvent = require('./createEvent');
const deleteEvent = require('./deleteEvent');
const updateEvent = require('./updateEvent');
const eventFilter = require('./eventFilter');
const { withFilter } = require('apollo-server-express');
const { pubsub } = require('../../context/pubsub')
const createEventWithInvite = require('./createEventWithInvite');
// const fileUpload = require('./fileUpload');
const userCreatedEvents = require('./userCreatedEvents');
const getUserById = require('../User/getUserById');
const { assertValidExecutionArguments } = require('graphql/execution/execute');

module.exports = {
  // user: {
  //   __resolveReference(ref) {
  //     return { id : 1};
  //   },
  // },
  Mutation: {
    createEvent,
    deleteEvent,
    updateEvent,
    createEventWithInvite,
    // fileUpload
  },
  Query: {
    eventFilter,
    userCreatedEvents
  },

  event: {
    async creator(event) {
      // console.log("starting");
      return { __typename: "user", id: event.userId };
     }
  }


  // Subscription: {
  // event: {
  //   subscribe: withFilter(
  //     () => pubsub.asyncIterator("EVENT"),
  //     (payload, args) => {
  //       console.log('EVENT SUBSCRIPTION CALLED')
  //       console.log(payload.event.data.dataValues.id)
  //       if(!args.id){
  //         return true;
  //       }else{
  //         if (payload.event.data.dataValues.id == args.id) {
  //           return true;
  //         }
  //         return false;
  //       }
  //     }
  //   ),
  // },
  // }
}