const { AuthenticationError } = require("apollo-server");
const { Event, GuestList, Sequelize, sequelize, User } = require("../../../database/models");

const createEventWithInvite = async (_, args, { req }) => {
  try {
    const { eventName, description, eventDate, guestEmails } = args.input;
    const t = await sequelize.transaction();
    const userCheck = await User.findOne({ where: { id: req.user.id } });

    if (!userCheck) {
      throw new AuthenticationError("user not available");
    }
    const result = async function () {
      const eventCreated = await Event.create({ userId: req.user.id, eventName, description, eventDate }, { transaction: t });
      for (let i = 0; i < guestEmails.length; i++) {
        const guestCheck = await User.findOne({ where: { email: guestEmails[i] } });
        if (!guestCheck) {
          await t.rollback();
          throw new AuthenticationError("Guest must register.");
        }
        const inviteCreated = await GuestList.create({ eventId: eventCreated.id, email: guestEmails[i] }, { transaction: t });
        if (i == 5) {
          await t.rollback();
          throw new AuthenticationError("only 5 guest emails are allowed.");
        }
      }
      await t.commit();
      return eventCreated;
    }
    return result;
  } catch (error) {
    await t.rollback();
    throw error;
  }
}

module.exports = createEventWithInvite;