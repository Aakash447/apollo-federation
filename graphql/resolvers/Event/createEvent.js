const { Event } = require("../../../database/models");
// const { pubsub } = require('../../context/pubsub');

const createEvent = async (_, args, { req }) => {
  try {
    const { eventName, description, eventDate } = args.input;
    const userId = req.user.id;
    // const userId = 2;

    const eventInstance = { userId, eventName, description, eventDate };
    const event = await Event.create(eventInstance);
    // pubsub.publish("EVENT", {
    //   event: {
    //     mutation: "CREATED",
    //     data: event,
    //   },
    // });
    return event;
  } catch (error) {
    throw error;
  }
}

module.exports = createEvent;