const { AuthenticationError } = require('apollo-server-express');
const { date } = require('joi');
const { Event } = require("../../../database/models");

const deleteEvent = async (_, args, { req }) => {
  try {
    const { eventId } = args.input;
    const checkEvent = await Event.findOne({ where: { id: eventId } });
    if (!checkEvent) {
      return new AuthenticationError('Event not found.')
    }
    if (checkEvent.userId != req.user.id) {
      return new AuthenticationError('Not Authorized');
    }
    checkEvent.deleteEventDate = new date();
    return await checkEvent.save();
  } catch (error) {
    throw error;
  }
}

module.exports = deleteEvent;