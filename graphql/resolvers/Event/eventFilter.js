const db = require('../../../database/models/index');


const eventFilter = async (parent, args, { req }) => {
  try {
    const { eventDate, eventEndDate, searchKeyword, sortOrder = 'ASC', sortByColumn = 'createdAt', offset = 0, limit = 10 } = args.input;

    let query = `SELECT * FROM "Events" AS "Event" WHERE 1 = 1`;

    if(searchKeyword) {
      query = `${query} AND "Event"."eventName" LIKE '%${searchKeyword}%'`;
    }
    if(req.user.id) {
      query = `${query} AND "Event"."userId"='${req.user.id}'`;
    }
    if(eventDate && eventEndDate) {
      query = `${query} AND "Event"."eventDate" BETWEEN '${eventDate}' AND '${eventEndDate}' `;
    }
    if(eventDate && !eventEndDate) {
      query = `${query} AND "Event"."eventDate"='${eventDate}'`;
    }

    query = `${query} ORDER BY "${sortByColumn}" ${sortOrder} OFFSET ${offset} LIMIT ${limit}`;

    return await db.sequelize.query(
      query,
      {
        type: db.sequelize.QueryTypes.SELECT
      }
    );
  } catch (error) {
    throw error;
  }
}

module.exports = eventFilter;