const userResolver = require('./User');
const eventResolver = require('./Event');
const GuestListResolver = require('./GuestList');

module.exports = [userResolver];
// module.exports = [userResolver, eventResolver, GuestListResolver];