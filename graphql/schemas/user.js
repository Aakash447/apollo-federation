const { gql } = require('apollo-server-express');

// module.exports = gql`
// enum CURRENCY_CODE {
//   USD
// }

// type Department {
//   category: ProductCategory
//   url: String
// }

// type Money {
//   amount: Float
//   currencyCode: CURRENCY_CODE
// }

// """Here are some helpful details about your type"""
// type Price {
//   cost: Money

//   """A number between 0 and 1 signifying the % discount"""
//   deal: Float
//   dealSavings: Money
// }

// """
// This is an Entity, docs:https://www.apollographql.com/docs/federation/entities/
// You will need to define a __resolveReference resolver for the type you define, docs: https://www.apollographql.com/docs/federation/entities/#resolving
// """
// type Product @key(fields: "id") {
//   id: ID!
//   title: String
//   url: String
//   description: String
//   price: Price
//   salesRank(category: ProductCategory = ALL): Int
//   salesRankOverall: Int
//   salesRankInCategory: Int
//   category: ProductCategory
//   images(size: Int = 1000): [String]
//   primaryImage(size: Int = 1000): String
// }

// enum ProductCategory {
//   ALL
//   GIFT_CARDS
//   ELECTRONICS
//   CAMERA_N_PHOTO
//   VIDEO_GAMES
//   BOOKS
//   CLOTHING
// }

// extend type Query {
//   bestSellers(category: ProductCategory = ALL): [Product]
//   categories: [Department]
//   product(id: ID!): Product
// }
// `;
module.exports = gql `
  type user @key(fields: "id"){
    # type user {
    id: Int
    name: String
    email: String
  }

  # extend type event @key(fields: "id") {
  #   id: Int @external
  #   creator: user
  # }

  type Mutation {
    registerUser(input: registerUserInput!): registerUserResponse,
    login(input: loginInput!): loginResponse,
    changePassword(input: changePasswordInput): changePasswordResponse,
    resetPassword(input: resetPasswordInput): changePasswordResponse
  }

  type Query {
    getUser(id: Int!) : registerUserResponse
  }

  input resetPasswordInput {
    email: String,
    newPassword: String,
    confirmNewPassword: String
  }

  input registerUserInput {
    name: String,
    email: String,
    password: String
  }

  type registerUserResponse {
    id: Int,
    name: String,
  }

  input loginInput {
    email: String,
    password: String
  }

  type loginResponse {
    name: String,
    email: String,
    token: String,
  }

  input changePasswordInput {
    oldPassword: String,
    newPassword: String,
    confirmNewPassword: String
  }

  type changePasswordResponse {
    id: Int,
    name: String,
    email: String
  }
`
