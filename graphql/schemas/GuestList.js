const { gql } = require('apollo-server-express');

module.exports = gql `
  type GuestList {
    id: Int,
    email: String,
    events: event
  }

  extend type Mutation {
    createInvite(input: createInviteInput): createInviteResponse @isAuth
  }

  extend type Query {
    getInvitedEventList: [getInvitedEventListResponse!] @isAuth
  }

  type getInvitedEventListResponse {
    email: String,
    events: event
  }

  input createInviteInput {
    eventId: Int
    email: String
  }

  type createInviteResponse {
    id: Int,
    email: String,
  }
`