const { gql } = require('apollo-server-express');
const userType = require('./user');
const eventType = require('./event');
const GuestListType = require('./GuestList');

const rootType = gql`
    scalar Date
    # directive @isAuth on FIELD_DEFINITION

    extend type Query {
        root: String
    }
    
    extend type Mutation {
        root: String
    }

    # type Subscription {
    #     root: String
    # }
`
module.exports = [rootType, userType];
// module.exports = [rootType, userType, eventType, GuestListType];
