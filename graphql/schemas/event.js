const { gql } = require('apollo-server-express');

module.exports = gql`

# extend type Product @key(fields: "id") {
#   id: ID! @external
#   reviews: [Review]
#   reviewSummary: ReviewSummary
# }

# """
# This is an Entity, docs:https://www.apollographql.com/docs/federation/entities/
# You will need to define a __resolveReference resolver for the type you define, docs: https://www.apollographql.com/docs/federation/entities/#resolving
# """
# type Review @key(fields: "id") {
#   id: ID!
#   rating: Float
#   content: String
# }

# type ReviewSummary {
#   totalReviews: Int
#   averageRating: Float
# }

    scalar Date

  type event @key(fields: "id") {
    id: Int,
    eventDate: Date,
    description: String,
    eventName: String,
    creator: user
  }
  extend type user @key(fields: "id") {
    id: Int @external
  }

  # type File {
  #   filename: String!,
  #   mimetype: String!,
  #   encoding: String!,
  #   uri: String,
  # }

  type Mutation {
    createEvent(input: createEventInput): createEventResponse
    # createEventWithInvite(input: createEventWithInviteInput): event!
    # deleteEvent(input: deleteEventInput): deleteEventResponse
    # updateEvent(input: updateEventInput): createEventResponse 
    # fileUpload(file: Upload!): File!
  }

  type Query {
    eventFilter(input: eventFilterInput): [event!] 
    userCreatedEvents: [event] 
  }

  type response {
    id: Int,
    eventName: String,
    # guests: [String]
  }

  # extend type Subscription {
  #   event(id: Int): subscriptionPayload! 
  # }

  input createEventWithInviteInput {
    eventName: String,
    description: String,
    eventDate: Date,
    guestEmails: [String!]!
  }

  type subscriptionPayload {
    mutation: String!
    data: event!
  }

  input dateFilterInput {
    startDate: Date,
    endDate: Date
  }
  input eventFilterInput {
    eventDate: Date,
    eventName: String,
    sort: String,
    eventEndDate: Date,
    limit: Int,
    offset: Int,
    sortByColumn: String,
    sortOrder: String,
    searchKeyword: String,
  }
  input filterEventInput{
    startDate: Date,
    endDate: Date,
    limit: Int,
    offset: Int
  }

  input createEventInput {
    eventName: String,
    description: String,
    eventDate: Date,
  }

  type createEventResponse {
    eventName: String,
    description: String,
    eventDate: Date
  }

  input deleteEventInput {
    eventId: Int,
    eventName: String
  }

  type deleteEventResponse{
    id: Int
  }

  input updateEventInput {
    eventId: Int,
    newEventName: String,
    newDescription: String
  }
`